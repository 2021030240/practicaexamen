/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicaexamen;

/**
 *
 * @author _Windows_
 */
public class RegistroVenta {
    private int codVenta;
    private int cantidad;
    private int tipo;
    private float precio;
    
    //Metodos
    //Metodos COnstructor
    public RegistroVenta(){ //Constructor por omision
        this.codVenta=0;
        this.cantidad=0;
        this.tipo=0;
        this.precio=0.0f;
    }
    public RegistroVenta(int codVenta, int cantidad, int tipo, float precio){ //Constructor por asignacion
        this.codVenta = codVenta;
        this.cantidad = cantidad;
        this.tipo = tipo;
        this.precio = precio;
    }
    public RegistroVenta(RegistroVenta otro){ //Constructor por copia
        this.codVenta = otro.codVenta;
        this.cantidad = otro.cantidad;
        this.tipo = otro.tipo;
        this.precio = otro.precio;
    }

    //Metodos set y get

    public int getCodVenta() {
        return codVenta;
    }

    public void setCodVenta(int codVenta) {
        this.codVenta = codVenta;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    public float calcularCostoV(){
        return this.cantidad * this.precio;
    }
    public float calcularImpuesto(){
        return this.calcularCostoV()*0.16f;
    }
    public float calcularTotalp(){
        return this.calcularCostoV() + this.calcularImpuesto();
    }
}